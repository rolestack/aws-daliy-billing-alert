import json
import boto3
import urllib
import gspread
import smtplib, ssl
from botocore.exceptions import ClientError
from typing import Optional, Tuple
from datetime import date, timedelta
from decimal import Decimal
from traceback import format_exc
from oauth2client.service_account import ServiceAccountCredentials
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header

def lambda_handler(event, context):
# def run():

    # 이메일 발신자 정의
    SENDER_EMAIL = "rolestack@gmail.com"
    EMAIL_SECRET_ARN = "arn:aws:secretsmanager:ap-northeast-2:123412341234:secret:billing-sender-email-password-9uCBDX"
    SENDER_PASSWORD = str(get_email_secret(EMAIL_SECRET_ARN))
    
    # AWS Secret Manager의 데이터 가져오기
    CUSTOMER_SECRET_ARN = "arn:aws:secretsmanager:ap-northeast-2:123412341234:secret:billing-credentials-key-V588Ae"
    SECRET_DATA = get_customer_secret(CUSTOMER_SECRET_ARN)
    
    # Google Spread Sheet의 데이터 가져오기
    DATA_SHEET, ADMIN_DATA_SHEET = get_spread_sheet_data()
    
    # 모든 stdout을 관리자에게 전송할 데이터입니다.
    admin_logs = ""

    # 메일 제목 정의
    mail_subject = "[EOCIS] 일일 AWS 비용 알림"

    for customer in range(len(SECRET_DATA)):
        # Customer 가독성 추가
        CUSTOMER = SECRET_DATA[customer]["Customer"].replace("-"," - ", 1)
        ACCOUNT = SECRET_DATA[customer]["AWS_ACCOUNT_ID"]
        ACCESS_KEY = SECRET_DATA[customer]["ACCESS_KEY"]
        SECRET_KEY = SECRET_DATA[customer]["SECRET_ACCESS_KEY"]
        
        result = {"daily": None, "monthly": None, "premonth": None}
        
        ce = boto3.client(
            'ce',
            aws_access_key_id=ACCESS_KEY,
            aws_secret_access_key=SECRET_KEY,
        )
        
        for key, date_range in {
            "daily": get_daily_range(),
            "monthly": get_current_month_range(),
            "premonth": get_pre_month_range(),
        }.items():
            try:
                result[key] = {"isSuccess": True, "data": get_cost(ce, date_range)}
            except Exception as e:
                result[key] = {
                    "isSuccess": False,
                    "error": {"message": str(e), "stacktrace": format_exc()},
                }

        print(result)

        admin_logs = admin_logs + "\n=============================\n"

        # Secret Manager와 Google Sheet에 있는값을 서로 검사하여 알림 발송
        # Secret Manager에 등록된 Account 개수만큼 반복
        for alert_count in range(len(SECRET_DATA)):
            try:
                if ACCOUNT != DATA_SHEET[alert_count][0].get("AWS Account ID"):
                    alert_count = alert_count + 1
                elif ACCOUNT == DATA_SHEET[alert_count][0].get("AWS Account ID") and DATA_SHEET[alert_count][0].get("Notification") == "TRUE":
                    admin_logs = admin_logs + f"\n*{DATA_SHEET[alert_count][0].get('Customer')}*  |  AWS Secret Manager({ACCOUNT}) is match Spread Sheet({DATA_SHEET[alert_count][0].get('AWS Account ID')})\n"
                    # email 포함한 모든 알림 전송
                    if DATA_SHEET[alert_count][0].get("Notification Format").find("slack") != -1 and DATA_SHEET[alert_count][0].get("Notification Format").find("email") != -1:
                        admin_logs = admin_logs + "\nNotification Type: slack & email\n"
                        admin_logs = admin_logs + "\n----Slack MESSAGE----\n"
                        slack_message = create_slack_message(result, CUSTOMER, ACCOUNT) 
                        admin_logs = admin_logs + slack_message
                        admin_logs = admin_logs + "\n----Email MESSAGE----\n"
                        email_message = create_email_message(result, CUSTOMER, ACCOUNT)
                        admin_logs = admin_logs + email_message
                        webhook_url = DATA_SHEET[alert_count][0].get("webhook url for alert")
                        to_email = DATA_SHEET[alert_count][0].get("email for alert")
                        post_to_slack(message=slack_message,url=webhook_url)
                        post_to_email(sender=SENDER_EMAIL,sender_password=SENDER_PASSWORD,message=email_message,email=to_email,subject=mail_subject)
                        break
                    # Slack 알림 전송
                    elif DATA_SHEET[alert_count][0].get("Notification Format").find("slack") != -1:
                        admin_logs = admin_logs + "\nNotification Type: slack\n"
                        admin_logs = admin_logs + "\n----MESSAGE----\n"
                        slack_message = create_slack_message(result, CUSTOMER, ACCOUNT)        
                        admin_logs = admin_logs + slack_message
                        webhook_url = DATA_SHEET[alert_count][0].get("webhook url for alert")
                        post_to_slack(message=slack_message,url=webhook_url)
                        break
                    # Email 알림 전송
                    elif DATA_SHEET[alert_count][0].get("Notification Format").find("email") != -1:
                        admin_logs = admin_logs + "\nNotification Type: email\n"
                        admin_logs = admin_logs + "\n----MESSAGE----\n"
                        email_message = create_email_message(result, CUSTOMER, ACCOUNT)
                        admin_logs = admin_logs + email_message
                        to_email = DATA_SHEET[alert_count][0].get("email for alert")
                        post_to_email(sender=SENDER_EMAIL,sender_password=SENDER_PASSWORD,message=email_message,email=to_email,subject=mail_subject)
                        break
                    else:
                        admin_logs = admin_logs + f"\n*{DATA_SHEET[alert_count][0].get('Customer')}* |  ERROR 1: 알림 전송 플랫폼 정의 오류\n"
                        break
                elif ACCOUNT == DATA_SHEET[alert_count][0].get("AWS Account ID") and DATA_SHEET[alert_count][0].get("Notification") == "FALSE":
                    admin_logs = admin_logs + f"\n{DATA_SHEET[alert_count][0].get('Customer')}  |  AWS Secret Manager({ACCOUNT}) is match Spread Sheet({DATA_SHEET[alert_count][0].get('AWS Account ID')})\n"
                    admin_logs = admin_logs + f"\nINFO_{DATA_SHEET[alert_count][0].get('Customer')}: 알림 비활성화\n"
                    break
                else:
                    admin_logs = admin_logs + f"\n`{DATA_SHEET[alert_count][0].get('Customer')}` |  ERROR 2: 데이터간 정합성 확인 오류\n"
            except Exception as e:
                for send in range(len(ADMIN_DATA_SHEET)):
                    if ADMIN_DATA_SHEET[send][0].get("Notification") == "TRUE":
                        if ADMIN_DATA_SHEET[send][0].get("Notification Format") == "slack":
                            webhook_url = ADMIN_DATA_SHEET[send][0].get("webhook url for alert")
                            post_to_slack(message=f'{DATA_SHEET[alert_count][0].get("AWS Account ID")} error\n {e}',url=webhook_url)
                        elif ADMIN_DATA_SHEET[send][0].get("Notification Format") == "email":
                            to_email = ADMIN_DATA_SHEET[send][0].get("email for alert")
                            post_to_email(message=f'{DATA_SHEET[alert_count][0].get("AWS Account ID")} error',email=to_email)
                        elif "slack" in ADMIN_DATA_SHEET[send][0].get("Notification Format") and "email" in ADMIN_DATA_SHEET[send][0].get("Notification Format"):
                            webhook_url = ADMIN_DATA_SHEET[send][0].get("webhook url for alert")
                            to_email = ADMIN_DATA_SHEET[send][0].get("email for alert")
                            post_to_slack(message=f'{DATA_SHEET[alert_count][0].get("AWS Account ID")} error\n {e}',url=webhook_url)
                            post_to_email(message=f'{DATA_SHEET[alert_count][0].get("AWS Account ID")} error\n {e}',email=to_email)
                        else:
                            admin_logs = admin_logs + "\nERROR 3: 관리자 정보 에러\n"
                    else:
                        admin_logs = admin_logs + f"INFO_{ADMIN_DATA_SHEET[send][0].get('Receiver')}_Notification : OFF"
                        pass
    for send in range(len(ADMIN_DATA_SHEET)):
        if ADMIN_DATA_SHEET[send][0].get("Notification") == "TRUE":
            if ADMIN_DATA_SHEET[send][0].get("Notification Format") == "slack":
                webhook_url = ADMIN_DATA_SHEET[send][0].get("webhook url for alert")
                post_to_slack(message=admin_logs,url=webhook_url)
            elif ADMIN_DATA_SHEET[send][0].get("Notification Format") == "email":
                to_email = ADMIN_DATA_SHEET[send][0].get("email for alert")
                post_to_email(message=admin_logs,email=to_email)
            elif "slack" in ADMIN_DATA_SHEET[send][0].get("Notification Format") and "email" in ADMIN_DATA_SHEET[send][0].get("Notification Format"):
                webhook_url = ADMIN_DATA_SHEET[send][0].get("webhook url for alert")
                to_email = ADMIN_DATA_SHEET[send][0].get("email for alert")
                post_to_slack(message=admin_logs,url=webhook_url)
                post_to_email(message=admin_logs,email=to_email)
            else:
                print("\nERROR 3: 관리자 정보 에러\n")
        else:
            print(f"INFO_{ADMIN_DATA_SHEET[send][0].get('Receiver')}_Notification : OFF")
            pass    
    return result
    
def get_email_secret(secret_arn: str):

    region_name = "ap-northeast-2"

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    get_secret_value_response = client.get_secret_value(SecretId=secret_arn)
    secret = get_secret_value_response['SecretString']
    
    return secret

def get_customer_secret(secret_arn: str):

    region_name = "ap-northeast-2"

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    get_secret_value_response = client.get_secret_value(SecretId=secret_arn)
    key = get_secret_value_response['SecretString']
    key = key.replace("\n","")
    key = key.replace(" ","")
    secret = json.loads(key)
    
    return secret

def get_spread_sheet_data():
    spreadsheet_url = 'https://docs.google.com/spreadsheets/d/1TdeD3kvBxrSEf7IBcQ76wmyWOKVQHcdTj_ssvshFOFw'
    JSON_KEY = 'gcp_credentials.json'

    scope = [
        'https://spreadsheets.google.com/feeds',
        'https://www.googleapis.com/auth/drive'
    ]

    credentials = ServiceAccountCredentials.from_json_keyfile_name(JSON_KEY, scope)
    googleCredentials = gspread.authorize(credentials)
    customerListDocs = googleCredentials.open_by_url(spreadsheet_url)
    customerListDocsSheet = customerListDocs.worksheet('info')
    adminListDocsSheet = customerListDocs.worksheet('admin')
    
    # info 시트 내용 가져오기
    allCustomerData = list()
    customerValues = customerListDocsSheet.get_all_values()
    # 카테고리가 있는 순서(AWS Account ID 등)
    customerCategory = 0

    for value in range(len(customerValues)):
        locals()['tmp_dict'] = dict()
        locals()['tmp_list'] = list()
        if value <= customerCategory:
            pass
        else:
            for cell in range(len(customerValues[customerCategory])):
                locals()['tmp_dict'][customerValues[customerCategory][cell]] = customerValues[value][cell]
            locals()['tmp_list'].append(locals()['tmp_dict'])
            allCustomerData.append(locals()['tmp_list'])
    
    # index-error 시트 내용 가져오기
    allAdminData = list()
    adminValues = adminListDocsSheet.get_all_values()
    
    # 카테고리가 있는 순서(AWS Account ID 등)
    adminCategory = 1

    for value in range(len(adminValues)):
        locals()['tmp_dict'] = dict()
        locals()['tmp_list'] = list()
        if value <= adminCategory:
            pass
        else:
            for cell in range(len(adminValues[adminCategory])):
                locals()['tmp_dict'][adminValues[adminCategory][cell]] = adminValues[value][cell]
            locals()['tmp_list'].append(locals()['tmp_dict'])
            allAdminData.append(locals()['tmp_list'])

    return allCustomerData, allAdminData
    
#################

def post_to_slack(message: str, url: str):
    """`message` : slack에 삽입할 메시지이며 메인 함수에서 데이터를 호출 및 가공합니다.
    `url` : slack webhook url을 입력합니다.
    """
    data = {"text": message}
    req = urllib.request.Request(
        url,
        json.dumps(data, ensure_ascii=False).encode(),
        {"Content-Type": "application/json"},
    )
    resp = urllib.request.urlopen(req)
    return resp

def post_to_email(sender: str, sender_password: str, message: str, email: str, subject: str):
    SMTP_SSL_PORT=465 # SSL connection
    SMTP_SERVER="smtp.gmail.com"
    
    # 여러 이메일 가공
    mail_list = email.split(",")
    processed_list = list()
    for i in mail_list:
        i = i + ";"
        processed_list.append(i.replace(" ", ""))
    to_processed_list = ", ".join(processed_list)

    send_message = MIMEMultipart()
    send_message['From'] = sender
    send_message['To'] = to_processed_list
    send_message['Subject'] = Header(subject, 'utf-8')

    message = MIMEText(message, 'html', 'utf-8')
    send_message.attach(message)


    context = ssl.create_default_context()

    with smtplib.SMTP_SSL(SMTP_SERVER, SMTP_SSL_PORT, context=context) as server:
        server.login(sender, sender_password)
        server.sendmail(sender, processed_list, send_message.as_string())

    return None
    
#################
def get_current_month_range() -> Tuple[date, date]:
    today = date.today()
    end_datetime = today
    start_datetime = (
        today.replace(day=1)
        if today.day > 1
        else (today - timedelta(days=1)).replace(day=1)
    )
    return start_datetime, end_datetime


def get_pre_month_range() -> Tuple[date, date]:
    end_date, _ = get_current_month_range()
    start_date = (end_date - timedelta(days=1)).replace(day=1)
    return start_date, end_date


def get_daily_range() -> Tuple[date, date]:
    end_date = date.today()
    start_date = end_date - timedelta(days=1)
    return start_date, end_date


def create_option(date_range: Tuple[date, date]) -> dict:
    return {
        "TimePeriod": {
            "Start": date_range[0].isoformat(),
            "End": date_range[1].isoformat(),
        },
        "Filter": {
            "Not": {
                "Dimensions": {
                    "Key": "RECORD_TYPE",
                    "Values": ["Refund", "Credit"]
                }
            }
        },
        "Granularity": "MONTHLY",
        # 크레딧 적용후 금액은 아래 메트릭을 사용
        # "Metrics": ["AmortizedCost"],
        # 모든 메트릭은 아래와 같음
        # "Metrics" : ["BLENDED_COST", "UNBLENDED_COST", "AMORTIZED_COST", "NET_AMORTIZED_COST", "NET_UNBLENDED_COST", "USAGE_QUANTITY", "NORMALIZED_USAGE_AMOUNT"]
        "Metrics" : ["UNBLENDED_COST"]
    }


def execute_get_cost(option: dict, ce) -> dict:
    resp = ce.get_cost_and_usage(**option)
    print(resp)
    return {
        "start": resp["ResultsByTime"][0]["TimePeriod"]["Start"],
        "end": resp["ResultsByTime"][0]["TimePeriod"]["End"],
        "billing": resp["ResultsByTime"][0]["Total"]["UnblendedCost"]["Amount"],
        "unit": resp["ResultsByTime"][0]["Total"]["UnblendedCost"]["Unit"],
    }


def get_cost(ce, range: Tuple[date, date]):
    option = create_option(range)
    return execute_get_cost(option, ce)
    
def create_slack_message(data: dict, CUSTOMER: str, ACCOUNT: str) -> str:
    message_account = None
    message_premonth = None
    message_daily = None
    message_monthly = None

    premonth = None
    month = None
    daily = None
    
    account_name = f"{CUSTOMER}({ACCOUNT})"
    if account_name:
        message_account = f"계정명: {account_name}"
    if data["premonth"]["isSuccess"]:
        message_premonth = "지난 달: {0} {1} ({2}〜{3})".format(
            round(float(data["premonth"]["data"]["billing"]), 2),
            data["premonth"]["data"]["unit"],
            data["premonth"]["data"]["start"],
            data["premonth"]["data"]["end"],
        )
        premonth = Decimal(data["premonth"]["data"]["billing"])
    if data["monthly"]["isSuccess"]:
        message_monthly = "이번 달: {0} {1} ({2}〜{3})".format(
            round(float(data["monthly"]["data"]["billing"]), 2),
            data["monthly"]["data"]["unit"],
            data["monthly"]["data"]["start"],
            data["monthly"]["data"]["end"],
        )
        month = Decimal(data["monthly"]["data"]["billing"])
        if premonth:
            rate = int((month / premonth) * 10000) / 100
            message_monthly += f" (지난 달 대비: {rate}%)"
    if data["daily"]["isSuccess"]:
        message_daily = "어제: {0} {1} ({2}〜{3})".format(
            round(float(data["daily"]["data"]["billing"]), 2),
            data["daily"]["data"]["unit"],
            data["daily"]["data"]["start"],
            data["daily"]["data"]["end"],
        )
        daily = Decimal(data["daily"]["data"]["billing"])
        if month:
            rate = int((daily / month) * 10000) / 100
            message_daily += f" (이번 달 총비: {rate}%)"
        if premonth:
            rate = int((daily / premonth) * 10000) / 100
            message_daily += f" (지난 달 총비: {rate}%)"
    message = "\n".join(
        [
            x
            for x in ['<!channel>', message_account, message_monthly, message_premonth, message_daily]
            if x
        ]
    )
    return message

def create_email_message(data: dict, CUSTOMER: str, ACCOUNT: str) -> str:
    message_account = None
    message_premonth = None
    message_daily = None
    message_monthly = None

    premonth = None
    month = None
    daily = None
    account_name = f"{CUSTOMER}({ACCOUNT})"
    if account_name:
        message_account = f"{account_name}"
    if data["premonth"]["isSuccess"]:
        message_premonth = "지난 달: {0} {1} ({2}〜{3})".format(
            round(float(data["premonth"]["data"]["billing"]), 2),
            data["premonth"]["data"]["unit"],
            data["premonth"]["data"]["start"],
            data["premonth"]["data"]["end"],
        )
        premonth = Decimal(data["premonth"]["data"]["billing"])
    if data["monthly"]["isSuccess"]:
        message_monthly = "이번 달: {0} {1} ({2}〜{3})".format(
            round(float(data["monthly"]["data"]["billing"]), 2),
            data["monthly"]["data"]["unit"],
            data["monthly"]["data"]["start"],
            data["monthly"]["data"]["end"],
        )
        month = Decimal(data["monthly"]["data"]["billing"])
        if premonth:
            rate = int((month / premonth) * 10000) / 100
            message_monthly += f" (지난 달 대비: {rate}%)"
    if data["daily"]["isSuccess"]:
        message_daily = "어제: {0} {1} ({2}〜{3})".format(
            round(float(data["daily"]["data"]["billing"]), 2),
            data["daily"]["data"]["unit"],
            data["daily"]["data"]["start"],
            data["daily"]["data"]["end"],
        )
        daily = Decimal(data["daily"]["data"]["billing"])
        if month:
            rate = int((daily / month) * 10000) / 100
            message_daily += f" (이번 달 총비: {rate}%)"
        if premonth:
            rate = int((daily / premonth) * 10000) / 100
            message_daily += f" (지난 달 총비: {rate}%)"
    message = \
f"""
<html>
<head>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Nanum+Gothic&display=swap');

        h1 {{
            font-family: "Gothic", sans-serif;
            font-size: 15pt;
        }}
        h3 {{
            font-family: "Gothic", sans-serif;
            font-size: 12pt;
        }}
        p {{
            font-family: "Gothic", sans-serif;
            font-size: 11pt;
            font-weight: normal;
        }}
        b {{
            font-family: "Gothic", sans-serif;
            font-size: 11pt;
        }}
    </style>
</head>
<body>
    <h1> AWS Account ID: {message_account} <h1>
    <hr />
    <p> {message_monthly} </p>
    <p> {message_premonth} </p>
    <p> {message_daily} </p>
</body>
</html>
"""
    return message

# run