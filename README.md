# AWS Billing Alert

![img](./img/diagram.png)

1. AWS EventBridge: cron을 통하여 매일 이벤트를 생성하여 Lambda를 트리거하는 역할을 맡습니다.
2. AWS Secrets Manager: 빌링 데이터를 조회하기 위한 각 IAM 사용자의 Secrets Key(시크릿키)를 저장하는 역할을 맡습니다.
3. Google Sheets(스프레드시트): 알림(Email, Slack)을 정의하는 데이터를 가지고 있으며 Lambda 함수에 의해 호출됩니다.
4. AWS Cost Explorer: AWS에 저장된 빌링 정보를 가지고있으며 Lambda 함수에 의해 호출됩니다.
5. AWS Lambda:
	1. Event Bridge에 의해 매일 함수가 트리거됩니다.
	2. 이후 Google Sheets와 Secrets Manager에 저장해둔 데이터를 가져오며 몇가지 조건이 있습니다.
		- Secrets Manager와 Google Sheets에 입력한 AWS Account의 개수가 동일해야합니다.
		- Secrets Manager에 저장하는 시크릿키는 Account의 별도의 계정을 생성하여 IAM 계정을 부여해야합니다.
	3. Google Sheets에 알림을 켜둔대상으로 Slack 또는 Email로 알림을 전송합니다.


### 스프레드시트와 AWS Secrets Manager를 분리한 이유는?

계정의 추가의 경우 Google Sheets와 AWS Secrets Manager 모두 작성해야하지만 유지보수를 할때는 보통 알림 수신여부 또는 수신대상이 변경되는 작업이 많아 해당작업은 스프레드시트에서 처리합니다.

IAM 사용자의 시크릿키를 보관하기에는 Google Sheets는 보안상 적합하지않아 AWS Secrets Manager에 보관합니다.

---

TIP. AWS Lambda 함수 작성시 Layer에 추가할 라이브러리 파일은 이름을 "python.zip"으로 업로드 해야 사용가능합니다.

---

#### 2022.12.19 변경사항

1. Google Sheets에 알림대상중 email을 여러 수신지로 사용할 수 있게 변경되었습니다.
	- a@example.com,b@example.com 형태와 같이 입력할 수 있습니다. (이메일간 구분은 콤마(,)를 사용하며 콤마 앞뒤간 공백이 있어도 정상적으로 동작합니다.)

2. 금액을 소수점 2번째 자리까지만 표시합니다.

---

#### 2023.01.05 변경사항

1. AWS Secrets Manager에서 "Customer" 데이터를 가져올때 띄워쓰기가 무시된채로 데이터를 가져오는 내용을 수정하여 ```첫 대시(-)``` 기준으로 대시 양옆에 띄워쓰기를 추가했습니다.  
(고객 이름에 대시가 필요할 때 사용합니다.)

2. AWS Credit을 사용하게되면 기존 코드로는 크레딧으로 상쇄된 금액만 표시되어 해당 부분 코드를 수정하여 크레딧 적용 전 금액을 표시하도록 반영했습니다.

3. 메일 전송시 메일 제목에 필요없는 내용을 제거하였습니다.

	AS-IS
	![img](./img/old_mail.png)

	TO-BE
	![img](./img/new_mail.png)